terraform {
  backend "s3" {
    bucket = "vprofile-kops-state22"
    key    = "Gitlab/terraform.tfstate"
    region = "us-east-1"
  }
}
